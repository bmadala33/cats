<?php

return [
    'oracle' => [
        'driver'         => 'oracle',
        'tns'            => env('DB_TNS', ''),
        'host'           => env('DB_HOST', '10.37.66.3'),
        'port'           => env('DB_PORT', '1521'),
        'database'       => env('DB_DATABASE', 'test12'),
        'username'       => env('DB_USERNAME', 'csscats'),
        'password'       => env('DB_PASSWORD', 'csscats'),
        'charset'        => env('DB_CHARSET', 'AL32UTF8'),
        'prefix'         => env('DB_PREFIX', ''),
        'prefix_schema'  => env('DB_SCHEMA_PREFIX', ''),
        'edition'        => env('DB_EDITION', 'ora$base'),
        'server_version' => env('DB_SERVER_VERSION', '12c'),
    ],
];
