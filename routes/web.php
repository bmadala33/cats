<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
/* Users */
Route::get('/users','UserController@index')->name('users.index');
Route::get('/users/data-table','UserController@userDataTable')->name('users.dt');
Route::get('/users/create','UserController@create')->name('users.create');
Route::post('/users/save','UserController@saveUser')->name('users.save');
Route::get('/users/{user_id}/edit','UserController@editUser')->name('users.edit');
Route::post('/users/{user_id}/update','UserController@updateUser')->name('users.update');
/* Responsibility */
Route::get('/responsibilities','ResponsibilityController@index')->name('responsibilities.index');
Route::get('/responsibilities/data-table','ResponsibilityController@responsibilityDataTable')->name('responsibilities.dt');
Route::get('/responsibilities/create','ResponsibilityController@create')->name('responsibilities.create');
Route::post('/responsibilities/save','ResponsibilityController@saveResponsibility')->name('responsibilities.save');
Route::get('/responsibilities/{responsibility_id}/edit','ResponsibilityController@editResponsibility')->name('responsibilities.edit');
Route::post('/responsibilities/{responsibility_id}/update','ResponsibilityController@updateResponsibility')->name('responsibilities.update');

/* User Responsibility */
Route::get('/user_responsibilities','UserResponsibilityController@index')->name('user_responsibilities.index');
Route::get('/user_responsibilities/data-table','UserResponsibilityController@userResponsibilityDataTable')->name('user_responsibilities.dt');
Route::get('/user_responsibilities/create','UserResponsibilityController@create')->name('user_responsibilities.create');
Route::post('/user_responsibilities/save','UserResponsibilityController@saveUserResponsibility')->name('user_responsibilities.save');
Route::get('/user_responsibilities/{user_responsibility_id}/edit','UserResponsibilityController@editUserResponsibility')->name('user_responsibilities.edit');
Route::post('/user_responsibilities/{user_responsibility_id}/update','UserResponsibilityController@updateUserResponsibility')->name('user_responsibilities.update');
