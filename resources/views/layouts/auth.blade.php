<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
        <title>CATS</title>
    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link href="{{ asset('css/app.css').'?v=3' }}" rel="stylesheet">

</head>
<body class="bg-gradient-primary">
    @yield('content')
</body>
<script src="{{ asset('js/app.js').'?v=1' }}"></script>
<script src="js/sb-admin-2.min.js"></script>
<script src="{{url('js/template.js')}}"></script>
</body>
</html>
