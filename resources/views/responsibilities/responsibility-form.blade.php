@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">Responsibility Form</div>
            <div class="card-body">
                <form method="post" action="{{$action}}">
                    @csrf
                    <input hidden name="responsibility_id" value="{{isset($responsibilityId)?$responsibilityId:''}}">
                    <div class="row">
                        <!-- Responsibility Key -->
                        <div class="col-md-3 form-group">
                            <label for="responsibility_key" class="col-form-label">Responsibility Key<sup><i class="fas fa-asterisk text-danger"></i></sup></label>
                            <input type="text" class="form-control @error('responsibility_key') is-invalid @enderror"
                                   name="responsibility_key" value="{{old('responsibility_key',isset($responsibility) ? $responsibility->responsibility_key :'')}}">
                            @error('responsibility_key')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <!-- Responsibility Desc -->
                        <div class="col-md-3 form-group">
                            <label for="responsibility_desc" class="col-form-label">Description<sup><i class="fas fa-asterisk text-danger"></i></sup></label>
                            <input type="text" class="form-control @error('responsibility_desc') is-invalid @enderror"
                                   name="responsibility_desc" value="{{old('responsibility_desc',isset($responsibility) ? $responsibility->responsibility_desc :'')}}">
                            @error('responsibility_desc')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <!-- Start Date -->
                        <div class="col-md-3 form-group">
                            <label for="start_date" class="col-form-label">Start Date</label>
                            <input type="text" class="form-control date @error('start_date') is-invalid @enderror" autocomplete="off"
                                   name="start_date" value="{{old('start_date',isset($responsibility)? custom_date_format($responsibility->start_date) :'')}}">
                            @error('start_date')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <!-- End Date -->
                        <div class="col-md-3 form-group">
                            <label for="end_date" class="col-form-label">End Date</label>
                            <input type="text" class="form-control date @error('end_date') is-invalid @enderror" autocomplete="off"
                                   name="end_date" value="{{old('end_date',isset($responsibility) ? custom_date_format($responsibility->end_date)  :'')}}">
                            @error('end_date')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-md-12 form-group">
                            <button type="submit" class="btn btn-primary">{{isset($responsibilityId) ? 'Update' :'Create'}}</button>
                            <a href="{{route('responsibilities.index')}}" class="btn btn-primary">Close</a>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function(){
            $( ".date" ).datepicker();
        });
    </script>
@endpush