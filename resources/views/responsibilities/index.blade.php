@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">CATS Responsibilities
        		 <a href="{{route('responsibilities.create')}}" class="btn btn-primary float-right">Create Responsibility</a>
				    </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-bordered table-striped" id="responsibilityTable" width="100%">
                        <thead>
                        <tr>
                            <th>Responsibility Key</th>
                            <th>Description</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            let table = $('#responsibilityTable').DataTable({
                dom: 'lfBprtip',
                buttons: [
                    {
                        text: 'Reset',
                        action: function (e, dt, node, config) {
                            table.search('').draw();
                        }
                    }
                ],
                scrollX: true,
                orderCellsTop: true,
                processing: true,
                serverSide: true,
                responsive: true,
                pagingType: 'simple_numbers',
                lengthMenu: [[10, 25, 50, 75, 100, -1], [10, 25, 50, 75, 100, "All"]],
                ajax: {
                    url: "{{route('responsibilities.dt')}}",
                },
                columns: [
                    {data: 'responsibility_key', name: 'responsibility_key'},
                    {data: 'responsibility_desc', name: 'responsibility_desc'},
                    {data: 'start_date', name: 'start_date'},
                    {data: 'end_date', name: 'end_date'},
                    {data: 'action', name: 'action', searchable: false, orderable: false},
                ]
            });
        });
    </script>
@endpush
