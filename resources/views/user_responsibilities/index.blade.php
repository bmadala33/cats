@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-header">CATS User Responsibilities
        <a href="{{route('user_responsibilities.create')}}" class="btn btn-primary float-right">Assign Responsibility</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-sm table-bordered table-striped"  width="100%" id="userResponsibilityTable">
                    <thead>
                    <tr>
                        <th>User Name</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Responsibility Key</th>
                        <th>Description</th>
                        <!-- <th>User Type</th> -->
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    {{--<tbody>--}}

                      {{--@if(count($userResponsibilities)==0)--}}
                          {{--<tr>--}}
                              {{--<td colspan="7" class="text-center">No Records Found</td>--}}
                          {{--</tr>--}}
                      {{--@endif--}}


                      {{--@foreach($userResponsibilities as $userResponsibility)--}}
                          {{--<tr>--}}
                              {{--<td>{{$userResponsibility->user_name}}</td>--}}
                              {{--<td>{{$userResponsibility->first_name}}</td>--}}
                              {{--<td>{{$userResponsibility->last_name}}</td>--}}
                              {{--<td>{{$userResponsibility->responsibility_key}}</td>--}}
                              {{--<td>{{$userResponsibility->responsibility_desc}}</td>--}}
                              {{--<td>{{$userResponsibility->usr_resp_start_date}}</td>--}}
                              {{--<td>{{$userResponsibility->usr_resp_end_date}}</td>--}}
                              {{--<td><a href="{{route('user_responsibilities.edit',$userResponsibility->user_resp_id)}}" >Edit</a></td>--}}
                          {{--</tr>--}}
                      {{--@endforeach--}}

                    {{--</tbody>--}}
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            let table = $('#userResponsibilityTable').DataTable({
                dom: 'lfBprtip',
                buttons: [
                    {
                        text: 'Reset',
                        action: function (e, dt, node, config) {
                            table.search('').draw();
                        }
                    }
                ],
                scrollX: true,
                orderCellsTop: true,
                processing: true,
                serverSide: true,
                responsive: true,
                pagingType: 'simple_numbers',
                lengthMenu: [[10, 25, 50, 75, 100, -1], [10, 25, 50, 75, 100, "All"]],
                ajax: {
                    url: "{{route('user_responsibilities.dt')}}",
                },
                columns: [
                    {data: 'user_name', name: 'user_name'},
                    {data: 'first_name', name: 'first_name'},
                    {data: 'last_name', name: 'last_name'},
                    {data: 'responsibility_key', name: 'responsibility_key'},
                    {data: 'responsibility_desc', name: 'responsibility_desc'},
                    {data: 'usr_resp_start_date', name: 'usr_resp_start_date'},
                    {data: 'usr_resp_end_date', name: 'usr_resp_end_date'},
                    {data: 'action', name: 'action', searchable: false, orderable: false},
                ]
            });
        });
    </script>
@endpush
