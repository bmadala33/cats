@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">Assign User Responsibility Form</div>
            <div class="card-body">
                <form method="post" action="{{$action}}">
                    @csrf
                    <input type="hidden" name="user_responsibility_id" value="{{isset($userResponsibilityId)?$userResponsibilityId:''}}">
                    <div class="row">
                        <!-- user id -->
                        @php
                            $userValue = old('user_id',isset($userResponsibility) ? $userResponsibility->user_id :'');
                        @endphp
                        <div class="col-md-3 form-group">
                            <label for="userId" class="col-form-label">User <sup><i class="fas fa-asterisk text-danger"></i></sup></label>
                            <select class="form-control @error('user_id') is-invalid @enderror" id="userId"
                                    name="user_id">
                                <option value="">select an option</option>
                                @foreach($users as $user)
                                    <option value="{{$user->id}}" {{ $userValue == $user->id ? 'selected' :'' }}>{{$user->first_name.' '.$user->last_name}}</option>
                                @endforeach
                            </select>
                            @error('user_id')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <!-- responsibility id -->
                        @php
                            $responsibilityValue =old('responsibility_id',isset($userResponsibility) ? $userResponsibility->responsibility_id :'');
                        @endphp
                        <div class="col-md-3 form-group">
                            <label for="responsibility_id" class="col-form-label">Responsibility<sup><i class="fas fa-asterisk text-danger"></i></sup></label>

                            <select class="form-control @error('responsibility_id') is-invalid @enderror" id="responsibilityId"
                                    name="responsibility_id">
                                <option value="">select an option</option>
                                @foreach($responsibilities as $responsibility)
                                    <option value="{{$responsibility->id}}" {{ $responsibilityValue == $responsibility->id ? 'selected' :'' }}>{{$responsibility->responsibility_desc}}</option>
                                @endforeach
                            </select>
                            @error('responsibility_id')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">

                        <!-- Start Date -->
                        <div class="col-md-3 form-group">
                            <label for="usr_resp_start_date" class="col-form-label">Start Date</label>
                            <input type="text"
                                   class="form-control date @error('usr_resp_start_date') is-invalid @enderror"
                                   name="usr_resp_start_date"
                                   value="{{old('usr_resp_start_date',isset($userResponsibility)? custom_date_format($userResponsibility->usr_resp_start_date) :'')}}">
                            @error('usr_resp_start_date')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <!-- End Date -->
                        <div class="col-md-3 form-group">
                            <label for="usr_resp_end_date" class="col-form-label">End Date</label>
                            <input type="test"
                                   class="form-control date @error('usr_resp_end_date') is-invalid @enderror"
                                   name="usr_resp_end_date"
                                   value="{{old('usr_resp_end_date',isset($userResponsibility) ? custom_date_format($userResponsibility->usr_resp_end_date) :'')}}">
                            @error('usr_resp_end_date')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-md-12 form-group">
                            <button type="submit" class="btn btn-primary">{{isset($userResponsibilityId)? 'Update':'Create'}}</button>
                            <a href="{{route('user_responsibilities.index')}}" class="btn btn-primary">Close</a>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            $(".date").datepicker();
        });
    </script>
@endpush