@extends('layouts.auth')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10 col-lg-12 col-md-10">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image bg-primary"
                                 style="background-image: url('{{asset('/images/wdacs-logo.png')}}')"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <img class="mb-4 d-none d-login-icon img-fluid"
                                             src="{{asset('/images/wdacs-logo-desc.png')}}">

                                    </div>
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4 d-none d-lg-block">Login</h1>
                                    </div>
                                    <form class="user" method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <!-- Email Address -->
                                        <div class="form-group">
                                            <input id="user_name" type="text" placeholder="User Name"
                                                   class="form-control form-control-user {{ $errors->has('user_name') ? ' is-invalid' : '' }}"
                                                   name="user_name" value="{{ old('user_name') }}" required autofocus>
                                            @if ($errors->has('user_name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('user_name') }}</strong>
                                                </span>
                                            @endif
                                            @if (session('status'))
                                                <span class="text-success">
                                                    {{ session('status') }}
                                                </span>
                                            @endif
                                        </div>

                                        <!-- password -->
                                        <div class="form-group">
                                            <input id="password" type="password" placeholder="Password"
                                                   class="form-control form-control-user {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                   name="password" required>
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <!-- Remember Me -->
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" name="remember"
                                                       id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                <label class="custom-control-label" for="remember">Remember Me</label>
                                            </div>
                                        </div>

                                        <!-- Login button -->
                                        <button type="submit" class="btn btn-primary btn-user btn-block">
                                            {{ __('Login') }}
                                        </button>
                                    </form>
                                    <hr class="mb-5">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection