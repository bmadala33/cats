@extends('layouts.auth')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10 col-lg-12 col-md-9">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-password-image bg-primary"
                                 style="background-image: url('{{asset('/images/wdacs-logo.png')}}');"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <img class="mb-4 d-none d-login-icon img-fluid" src="{{asset('/images/wdacs-logo-desc.png')}}">
                                    </div>
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-2">Reset Password?</h1>
                                    </div>
                                    <form class="user" method="POST" action="{{ route('password.update') }}">
                                        @csrf
                                        <input type="hidden" name="token" value="{{ $token }}">
                                        <!-- Email Address -->
                                        <div class="form-group">
                                            <input id="email" type="email" placeholder="Enter Email Address..."
                                                   class="form-control form-control-user {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                   name="email" value="{{ $email ?? old('email') }}" required autofocus>
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <!-- Password -->
                                        <div class="form-group">
                                            <input id="password" type="password" placeholder="Password"
                                                   class="form-control form-control-user {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                   name="password" required>
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <!-- confirm Password -->
                                        <div class="form-group">
                                            <input id="password-confirm" type="password" placeholder="Confirm Password"
                                                   class="form-control form-control-user" name="password_confirmation"
                                                   required>
                                        </div>
                                        <div class="form-group">
                                            @if(session()->has('status'))
                                                <span class="valid-feedback d-block" role="alert">
                                                    <strong>{{ session()->get('status') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <!-- Password reset button -->
                                        <button type="submit" class="btn btn-primary btn-user btn-block">
                                            {{ __('Reset Password') }}
                                        </button>
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="{{route('register')}}">Create an Account!</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href="{{route('login')}}">Already have an account? Login!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
