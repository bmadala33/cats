@extends('layouts.auth')
@section('content')
    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-block bg-register-image bg-primary" style="background-image: url('{{asset('/images/wdacs-logo.png')}}')"></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <img class="mb-4 d-none d-login-icon img-fluid" src="{{asset('/images/wdacs-logo-desc.png')}}">
                            </div>
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4 d-none d-lg-block">Create Account</h1>
                            </div>
                            <form class="user" method="POST" action="{{ route('register') }}">
                            @csrf
                            <!-- User Name -->
                                <div class="form-group">
                                    <input id="username" placeholder="User Name" type="text"
                                           class="form-control form-control-user {{ $errors->has('username') ? ' is-invalid' : '' }}"
                                           name="username" value="{{ old('username') }}" required autofocus>
                                    @if ($errors->has('username'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <!-- First Name -->
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input id="firstname" type="text" placeholder="First Name"
                                               class="form-control form-control-user {{ $errors->has('firstname') ? ' is-invalid' : '' }}"
                                               name="firstname" value="{{ old('firstname') }}" required autofocus>
                                        @if ($errors->has('firstname'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('firstname') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <!-- Last Name -->
                                    <div class="col-sm-6">
                                        <input id="lastname" type="text" placeholder="Last Name"
                                               class="form-control form-control-user {{ $errors->has('lastname') ? ' is-invalid' : '' }}"
                                               name="lastname" value="{{ old('lastname') }}" required autofocus>
                                        @if ($errors->has('lastname'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('lastname') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <!-- Email -->
                                <div class="form-group">
                                    <input id="email" type="email" placeholder="Email Address"
                                           class="form-control form-control-user {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email" value="{{ old('email') }}" required>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group row">
                                    <!-- Password -->
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input id="password" type="password" placeholder="Password"
                                               class="form-control form-control-user{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                               name="password" required>
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-6">
                                        <input id="password-confirm" type="password" placeholder="Repeat Password"
                                               class="form-control form-control-user" name="password_confirmation" required>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    {{ __('Register Account') }}
                                </button>
                            </form>
                            <hr>
                            @if (Route::has('password.request'))
                                <div class="text-center">
                                    <a class="small" href="{{ route('password.request') }}">Forgot Password?</a>
                                </div>
                            @endif
                            <div class="text-center">
                                <a class="small" href="{{route('login')}}">Already have an account? Login!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

