@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">User Form</div>
            <div class="card-body">
                <form method="post" action="{{$action}}">
                    @csrf
                    <input type="hidden" name="user_id" value="{{isset($userId)?$userId:''}}">
                    @php
                        $routeName = \Route::currentRouteName();
                    @endphp
                    <input type="hidden" name="process_type"
                           value="{{$routeName == 'users.edit' ? 'UPDATE' :'CREATE'}}">
                    <div class="row">
                        <!-- User Name -->
                        <div class="col-md-3 form-group">
                            <label for="user_name" class="col-form-label">User Name</label>
                            <input type="text" class="form-control @error('user_name') is-invalid @enderror"
                                   name="user_name" value="{{old('user_name',isset($user) ? $user->user_name :'')}}">
                            @error('user_name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <!-- First Name -->
                        <div class="col-md-3 form-group">
                            <label for="first_name" class="col-form-label">First Name</label>
                            <input type="text" class="form-control @error('first_name') is-invalid @enderror"
                                   name="first_name" value="{{old('first_name',isset($user) ? $user->first_name :'')}}">
                            @error('first_name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <!-- Last Name -->
                        <div class="col-md-3 form-group">
                            <label for="last_name" class="col-form-label">Last Name</label>
                            <input type="text" class="form-control @error('last_name') is-invalid @enderror"
                                   name="last_name" value="{{old('last_name',isset($user) ? $user->last_name :'')}}">
                            @error('last_name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <!-- Email -->
                        <div class="col-md-3 form-group">
                            <label for="email" class="col-form-label">Email</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror"
                                   name="email" value="{{old('email',isset($user) ? $user->email  :'')}}">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <!-- Start Date -->
                        <div class="col-md-3 form-group">
                            <label for="start_date" class="col-form-label">Start Date</label>
                            <input type="text" class="form-control date @error('start_date') is-invalid @enderror" autocomplete="off"
                                   name="start_date" value="{{old('start_date',isset($user)? custom_date_format($user->start_date) :'')}}">
                            @error('start_date')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <!-- End Date -->
                        <div class="col-md-3 form-group">
                            <label for="end_date" class="col-form-label">End Date</label>
                            <input type="text" class="form-control date @error('end_date') is-invalid @enderror" autocomplete="off"
                                   name="end_date" value="{{old('end_date',isset($user) ? custom_date_format($user->end_date)  :'')}}">
                            @error('end_date')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <!-- Password -->
                        <div class="col-md-3 form-group">
                            <label for="password" class="col-form-label">{{ __('Password') }}</label>
                            <input id="password" type="password"
                                   class="form-control @error('password') is-invalid @enderror"
                                   name="password" autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="password-confirm" class="col-form-label">{{ __('Confirm Password') }}</label>
                            <input id="password-confirm" type="password" class="form-control"
                                   name="password_confirmation" autocomplete="new-password">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <button type="submit" class="btn btn-primary">Create</button>
                            <a href="{{route('users.index')}}" class="btn btn-primary">Close</a>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function(){
            $( ".date" ).datepicker();
        });
    </script>
@endpush