@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">CATS Users
                <a href="{{route('users.create')}}" class="btn btn-primary float-right">Create User</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-bordered table-striped" width="100%" id="userTable">
                        <thead>
                        <tr>
                            <th>User Name</th>
                            <th>Fist Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            let table = $('#userTable').DataTable({
                dom: 'lfBprtip',
                buttons: [
                    {
                        text: 'Reset',
                        action: function (e, dt, node, config) {
                            table.search('').draw();
                        }
                    }
                ],
                scrollX: true,
                orderCellsTop: true,
                processing: true,
                serverSide: true,
                responsive: true,
                pagingType: 'simple_numbers',
                lengthMenu: [[10, 25, 50, 75, 100, -1], [10, 25, 50, 75, 100, "All"]],
                ajax: {
                    url: "{{route('users.dt')}}",
                },
                columns: [
                    {data: 'user_name', name: 'user_name'},
                    {data: 'first_name', name: 'first_name'},
                    {data: 'last_name', name: 'last_name'},
                    {data: 'email', name: 'email'},
                    {data: 'start_date', name: 'start_date'},
                    {data: 'end_date', name: 'end_date'},
                    {data: 'action', name: 'action', searchable: false, orderable: false},
                ]
            });
        });
    </script>
@endpush
