<?php

namespace App\Providers;

use App\Contracts\UserInterface;
use App\Contracts\ResponsibilityInterface;
use App\Contracts\UserResponsibilityInterface;
use App\Repositories\UserRepository;
use App\Repositories\ResponsibilityRepository;
use App\Repositories\UserResponsibilityRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        /* User */
        $this->app->bind(
            UserInterface::class,
            UserRepository::class
        );

        /* Responsibility */

        $this->app->bind(
            ResponsibilityInterface::class,
            ResponsibilityRepository::class
        );

        /* User Responsibility */

        $this->app->bind(
            UserResponsibilityInterface::class,
            UserResponsibilityRepository::class
        );




    }


}
