<?php

namespace App\Http\Controllers;

use App\Contracts\UserInterface;
use App\Http\Requests\UserRequest;
use App\User;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    /**
     * @var UserInterface
     */
    private $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        $users = User::all();

        return view('users.index', ['users' => $users]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function userDataTable()
    {
        $users = User::query();

        return DataTables::of($users)
            ->filterColumn('start_date', function ($query, $keyword) {
                $query->whereRaw("TO_CHAR(start_date,'MM/DD/YYYY') like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('end_date', function ($query, $keyword) {
                $query->whereRaw("TO_CHAR(end_date,'MM/DD/YYYY') like ?", ["%{$keyword}%"]);
            })
            ->editColumn('start_date', function ($query) {
                return !empty($query->start_date) ? Carbon::parse($query->start_date)->format('m/d/Y') : '';
            })
            ->editColumn('end_date', function ($query) {
                return !empty($query->end_date) ? Carbon::parse($query->end_date)->format('m/d/Y') : '';
            })
            ->addColumn('action', function ($query) {
                return "<a href ='" . route('users.edit', [$query->id]) . "'><i class='fa fa-edit'>&nbsp;&nbsp;Edit</i></a>";

            })->make(true);
    }

    public function create()
    {
        $action = route('users.save');

        return view('users.user-form', ['action' => $action]);
    }

    public function saveUser(UserRequest $request)
    {
        $data = $request->all();
        //dd($this->user);
        $this->user->createOrUpdateUser($data, null, 'CREATE');

        return redirect()->to(route('users.index'));
    }

    public function editUser($userId)
    {
        $user = $this->user->getUser($userId);
        $action = route('users.update', $userId);

        return view('users.user-form', ['user' => $user, 'action' => $action,'userId'=>$userId]);
    }

    public function updateUser(UserRequest $request, $userId)
    {
        $data = $request->all();
        $this->user->createOrUpdateUser($data, $userId, 'UPDATE');

        return redirect()->to(route('users.index'));
    }
}
