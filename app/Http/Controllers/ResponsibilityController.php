<?php

namespace App\Http\Controllers;

use App\Contracts\ResponsibilityInterface;
use App\Http\Requests\ResponsibilityRequest;
use App\Models\Responsibility;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;

class ResponsibilityController extends Controller
{

    private $responsibility;

    public function __construct(ResponsibilityInterface $responsibility)
    {
        $this->responsibility = $responsibility;
    }



    public function index()
    {
        $responsibilities = Responsibility::all();

        return view('responsibilities.index', ['responsibilities' => $responsibilities]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function responsibilityDataTable()
    {
        $responsibilities = Responsibility::query();

        return DataTables::of($responsibilities)
            ->filterColumn('start_date', function ($query, $keyword) {
                $query->whereRaw("TO_CHAR(start_date,'MM/DD/YYYY') like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('end_date', function ($query, $keyword) {
                $query->whereRaw("TO_CHAR(end_date,'MM/DD/YYYY') like ?", ["%{$keyword}%"]);
            })
            ->editColumn('start_date', function ($query) {
                return !empty($query->start_date) ? Carbon::parse($query->start_date)->format('m/d/Y') : '';
            })
            ->editColumn('end_date', function ($query) {
                return !empty($query->end_date) ? Carbon::parse($query->end_date)->format('m/d/Y') : '';
            })
            ->addColumn('action', function ($query) {
                return "<a href ='" . route('responsibilities.edit', [$query->id]) . "'><i class='fa fa-edit'>&nbsp;&nbsp;Edit</i></a>";

            })->make(true);
    }

    public function create()
    {
        $action = route('responsibilities.save');

        return view('responsibilities.responsibility-form', ['action' => $action]);
    }

    public function saveResponsibility(ResponsibilityRequest $request)
    {
        $data = $request->all();
        // dd($data);
        //dd($this->responsibility);
        $this->responsibility->createOrUpdateResponsibility($data, null, 'CREATE');

        return redirect()->to(route('responsibilities.index'));
    }

    public function editResponsibility($responsibilityId)
    {
        $responsibility = $this->responsibility->getResponsibility($responsibilityId);
        // dd($responsibility);
        $action = route('responsibilities.update', $responsibilityId);
       // dd($action);
        return view('responsibilities.responsibility-form', [
            'responsibilityId'=>$responsibilityId,
            'responsibility' => $responsibility,
            'action' => $action]);
    }

    public function updateResponsibility(ResponsibilityRequest $request, $responsibilityId)
    {
        $data = $request->all();
        $this->responsibility->createOrUpdateResponsibility($data, $responsibilityId, 'UPDATE');

        return redirect()->to(route('responsibilities.index'));
    }
}
