<?php

namespace App\Http\Controllers;

use App\Contracts\UserResponsibilityInterface;
use App\Http\Requests\UserResponsibilityRequest;
use App\Models\Responsibility;
use App\Models\UserResponsibility;
use App\User;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;

class UserResponsibilityController extends Controller
{

    private $userResponsibility;

    public function __construct(UserResponsibilityInterface $userResponsibility)
    {
        $this->userResponsibility = $userResponsibility;
    }

    public function index()
    {
        $userResponsibilities = \DB::table('wdacs_cats_usr_resp_v')->get();

        return view('user_responsibilities.index', ['userResponsibilities' => $userResponsibilities]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function userResponsibilityDataTable()
    {
        $userResponsibilities = \DB::table('wdacs_cats_usr_resp_v')->select(['wdacs_cats_usr_resp_v.*']);

        return DataTables::of($userResponsibilities)
            ->filterColumn('usr_resp_start_date', function ($query, $keyword) {
                $query->whereRaw("TO_CHAR(usr_resp_start_date,'MM/DD/YYYY') like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('usr_resp_end_date', function ($query, $keyword) {
                $query->whereRaw("TO_CHAR(usr_resp_end_date,'MM/DD/YYYY') like ?", ["%{$keyword}%"]);
            })
            ->editColumn('usr_resp_start_date', function ($query) {
                return !empty($query->usr_resp_start_date) ? Carbon::parse($query->usr_resp_start_date)->format('m/d/Y') : '';
            })
            ->editColumn('usr_resp_end_date', function ($query) {
                return !empty($query->usr_resp_end_date) ? Carbon::parse($query->usr_resp_end_date)->format('m/d/Y') : '';
            })
            ->addColumn('action', function ($query) {
                return "<a href ='" . route('user_responsibilities.edit', [$query->user_resp_id]) . "'><i class='fa fa-edit'>&nbsp;&nbsp;Edit</i></a>";

            })->make(true);
    }

    public function create()
    {
        $action = route('user_responsibilities.save');

        $users =  User::all();
        $responsibilities = Responsibility::all();

        return view('user_responsibilities.user-responsibility-form', [
            'action' => $action,
            'users'=>$users,
            'responsibilities'=>$responsibilities]);
    }

    public function saveUserResponsibility(UserResponsibilityRequest $request)
    {
        $data = $request->all();
        //   dd($data);
        $this->userResponsibility->createOrUpdateUserResponsibility($data, null, 'CREATE');
        // dd($this->userResponsibility);
        return redirect()->to(route('user_responsibilities.index'));
    }

    public function editUserResponsibility($userResponsibilityId)
    {
        $users =  User::all();
        $responsibilities = Responsibility::all();
        $userResponsibility = $this->userResponsibility->getUserResponsibility($userResponsibilityId);
        // dd($userResponsibility);
        $action = route('user_responsibilities.update', $userResponsibilityId);
        // dd($action);
        return view('user_responsibilities.user-responsibility-form', [
            'userResponsibilityId'=>$userResponsibilityId,
            'userResponsibility' => $userResponsibility,
            'action' => $action,
            'responsibilities'=>$responsibilities,
            'users'=>$users]);
    }

    public function updateUserResponsibility(UserResponsibilityRequest $request, $userResponsibilityId)
    {
        $data = $request->all();
        $this->userResponsibility->createOrUpdateUserResponsibility($data, $userResponsibilityId, 'UPDATE');

        return redirect()->to(route('user_responsibilities.index'));
    }
}
