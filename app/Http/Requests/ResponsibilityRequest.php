<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResponsibilityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->request->get('responsibility_id')){
            return  [
                'responsibility_key' => ['required','unique:wdacs_cats_responsibility,responsibility_key,'.$this->request->get('responsibility_id')],
                'responsibility_desc' => ['required']
            ];
        }else{

            return  [
                'responsibility_key' => ['required','unique:wdacs_cats_responsibility,responsibility_key'],
                'responsibility_desc' => ['required']
            ];
        }
    }
}
