<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [

            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
        ];

        if ($this->request->get('user_id')) {
            $rules += [
                'user_name' => ['required', 'string', 'max:9', 'unique:wdacs_cats_user_info,user_name,' . $this->request->get('user_id')],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:wdacs_cats_user_info,email,' . $this->request->get('user_id'),
                'password' => ['required', 'string', 'min:8', 'confirmed']]
            ];
        } else {
            $rules += [
                'user_name' => ['required', 'string', 'max:9', 'unique:wdacs_cats_user_info'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:wdacs_cats_user_info']
            ];
        }

        return $rules;
    }
}
