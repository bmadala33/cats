<?php
/**
 * Created by PhpStorm.
 * User: C146652
 * Date: 3/5/2020
 * Time: 4:13 PM
 */

if (!function_exists('custom_date_format')) {
    function custom_date_format($date)
    {
        if (empty($date) || $date == '0000-00-00') {
            return '';
        }

        return \Carbon\Carbon::parse($date)->format('m/d/Y');
    }
}

if (!function_exists('custom_datetime_format')) {
    function custom_datetime_format($date)
    {
        if (empty($date) || $date == '0000-00-00 00:00:00') {
            return '';
        }

        return \Carbon\Carbon::parse($date)->format('m/d/Y g:i A');
    }
}