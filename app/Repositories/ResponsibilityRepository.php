<?php

namespace App\Repositories;


use App\Contracts\ResponsibilityInterface;
use App\Models\Responsibility;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ResponsibilityRepository implements ResponsibilityInterface
{

    private $responsibilityModel;

    public function __construct(Responsibility $responsibilityModel)
    {
        $this->responsibilityModel = $responsibilityModel;
    }

    public function createOrUpdateResponsibility(array $data, $responsibilityId = null, $processType)
    {
        $procedureName = 'WDACS_CATS_PKG.CREATE_R_UPDATE_RSPNSBLTY';
        $bindings = [
            'P_ID' => is_null($responsibilityId) ? 0 : $responsibilityId,
            'P_RESPONSIBILITY_KEY' => $data['responsibility_key'],
            'P_RESPONSIBILITY_DESC' => $data['responsibility_desc'],
            'P_START_DATE' => $data['start_date'],
            'P_END_DATE' => $data['end_date'],
            'P_CREATED_BY' => auth()->user()->user_name,
            'P_PROCESS_TYPE' => $processType,
        ];

        return DB::executeProcedure($procedureName, $bindings);
    }

    public function getResponsibility($responsibilityId)
    {
        return $this->responsibilityModel->find($responsibilityId);
    }
}
