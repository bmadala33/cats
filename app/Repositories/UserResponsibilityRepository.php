<?php

namespace App\Repositories;


use App\Contracts\UserResponsibilityInterface;
use App\Models\UserResponsibility;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserResponsibilityRepository implements UserResponsibilityInterface
{

    private $userResponsibilityModel;

    public function __construct(UserResponsibility $userResponsibilityModel)
    {
        $this->userResponsibilityModel = $userResponsibilityModel;
    }

    public function createOrUpdateUserResponsibility(array $data, $userResponsibilityId = null, $processType)
    {
        $procedureName = 'WDACS_CATS_PKG.CREATE_R_UPDATE_USR_RESP';
        $bindings = [
            'P_ID' => is_null($userResponsibilityId) ? 0 : $userResponsibilityId,
            'P_USER_ID' => $data['user_id'],
            'P_RESPONSIBILITY_ID' => $data['responsibility_id'],
            'P_START_DATE' => $data['usr_resp_start_date'],
            'P_END_DATE' => $data['usr_resp_end_date'],
            'P_CREATED_BY' => auth()->user()->user_name,
            'P_PROCESS_TYPE' => $processType,
        ];

        return DB::executeProcedure($procedureName, $bindings);
    }

    public function getUserResponsibility($userResponsibilityId)
    {
      //  return $this->userResponsibilityModel->find($userResponsibilityId);

      return   \DB::table("wdacs_cats_usr_resp_v")
                    ->where('user_resp_id','=',$userResponsibilityId)->first();

     /*
       return   \DB::table("wdacs_cats_usr_resp_v")
                                  ->where('user_resp_id','=',$userResponsibilityId)->get();
    */
    }
}
