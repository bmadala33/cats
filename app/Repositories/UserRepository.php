<?php

namespace App\Repositories;


use App\Contracts\UserInterface;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserInterface
{
    /**
     * @var User
     */
    private $userModel;

    public function __construct(User $userModel)
    {
        $this->userModel = $userModel;
    }

    public function createOrUpdateUser(array $data, $userId = null, $processType)
    {
        $procedureName = 'WDACS_CATS_PKG.CREATE_R_UPDATE_USER';
        $bindings = [
            'P_ID' => is_null($userId) ? 0 : $userId,
            'P_USER_NAME' => $data['user_name'],
            'P_FIRST_NAME' => $data['first_name'],
            'P_LAST_NAME' => $data['last_name'],
            'P_USER_TYPE' => 'WDACS_EMPLOYEE',
            'P_PASSWORD' => Hash::make($data['password']),
            'P_START_DATE' => $data['start_date'],
            'P_END_DATE' => $data['end_date'],
            'P_CREATED_BY' => auth()->user()->user_name,
            'P_EMAIL' => $data['email'],
            'P_PROCESS_TYPE' => $processType,
        ];

        return DB::executeProcedure($procedureName, $bindings);
    }

    public function getUser($userId)
    {
        return $this->userModel->find($userId);
    }
}