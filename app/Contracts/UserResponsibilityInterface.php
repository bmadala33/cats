<?php

namespace App\Contracts;


interface UserResponsibilityInterface
{
    public function createOrUpdateUserResponsibility(array $data, $userResponsibilityId = null, $processType);

    public function getUserResponsibility($userResponbilityId);
}
