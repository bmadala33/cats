<?php

namespace App\Contracts;


interface UserInterface
{
    public function createOrUpdateUser(array $data, $userId = null, $processType);

    public function getUser($userId);
}