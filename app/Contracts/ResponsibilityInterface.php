<?php

namespace App\Contracts;


interface ResponsibilityInterface
{
    public function createOrUpdateResponsibility(array $data, $responsibilityId = null, $processType);

    public function getResponsibility($responbilityId);
}
